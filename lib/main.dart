import 'package:calc/cubit/values_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider<ValuesCubit>(
        create: (BuildContext context) => ValuesCubit(),
        child: MyHomePage(title: 'Percentage Calculator'),
      ),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _percentageController;
  double percentage = 0;
  double currentPrice = 0;
  double increasedBy = 0;
  double targetPrice = 0;
  double from = 0;
  double to = 0;
  double percentageDifference = 0;
  double newPrice = 0;

  @override
  void initState() {
    super.initState();
    _percentageController = TextEditingController(text: 5.toString());
    percentage = 5;
  }

  // formula: price * (percent / 100)
  void updatePrices() {
    setState(() {
      increasedBy = (currentPrice * (percentage / 100));
      targetPrice = currentPrice + increasedBy;
    });
  }

  void updatePercentage() {
    setState(() {
      var percent = (to / from) * 100 - 100;
      percentageDifference = double.parse(percent.toStringAsFixed(2));
      print(percent);
      newPrice =
          double.parse((from + (from * (percent / 100))).toStringAsFixed(2));
    });
  }

  void copyAndToast(value) {
    var _value = value.toString();
    Clipboard.setData(
      new ClipboardData(
        text: _value,
      ),
    );
    final snackBar = SnackBar(
      content: Text('Copied'),
      duration: Duration(
        milliseconds: 300,
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        var values = context.watch<ValuesCubit>().state;

        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // debug
                // Column(
                //   children: values.entries.map((x) {
                //     return Container(
                //       color: Colors.grey,
                //       width: 250,
                //       child: Row(
                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //         children: [
                //           Text(x.key),
                //           Text(x.value.toString()),
                //         ],
                //       ),
                //     );
                //   }).toList(),
                // ),
                // calculator 1
                Text(
                  'What\'s the price increase/decrease?',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                  ),
                ),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 128,
                      child: TextField(
                        onChanged: (value) {
                          context.read<ValuesCubit>().updateCurrentPrice(value);
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          hintText: '100',
                          labelText: 'Current Price',
                          border: OutlineInputBorder(),
                          prefix: Text('\$'),
                        ),
                      ),
                    ),
                    SizedBox(width: 12),
                    Container(
                      width: 128,
                      child: TextField(
                        controller: _percentageController,
                        onChanged: (value) {
                          context.read<ValuesCubit>().updatePercentage(value);
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          hintText: _percentageController.text,
                          labelText: 'Percentage',
                          border: OutlineInputBorder(),
                          suffix: Text('%'),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Container(
                  width: 268,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Increased by:'),
                      SelectableText(
                        '\$${values["increasedBy"]}',
                        onTap: () => copyAndToast(values["increasedBy"]),
                        style: TextStyle(
                          color: values["increasedBy"] == 0
                              ? Colors.black
                              : values["increasedBy"] > 0
                                  ? Colors.green
                                  : Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                Container(
                  width: 268,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Target price:'),
                      SelectableText(
                        '\$${values["targetPrice"]}',
                        onTap: () => copyAndToast(values["targetPrice"]),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 24),
                Text(
                  'What\'s the percentage increase/decrease?',
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                  ),
                ),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 128,
                      child: TextField(
                        onChanged: (value) {
                          context.read<ValuesCubit>().updateFrom(value);
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          hintText: '100',
                          labelText: 'from',
                          border: OutlineInputBorder(),
                          prefix: Text('\$'),
                        ),
                      ),
                    ),
                    SizedBox(width: 12),
                    Container(
                      width: 128,
                      child: TextField(
                        onChanged: (value) {
                          context.read<ValuesCubit>().updateTo(value);
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          labelText: 'to',
                          border: OutlineInputBorder(),
                          prefix: Text('\$'),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Container(
                  width: 268,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Percentage difference:'),
                      SelectableText(
                        '${values["percentageDifference"]}%',
                        onTap: () =>
                            copyAndToast(values["percentageDifference"]),
                        style: TextStyle(
                          color: values["percentageDifference"] == 0
                              ? Colors.black
                              : values["percentageDifference"] > 0
                                  ? Colors.green
                                  : Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 12),
                Container(
                  width: 268,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('New price:'),
                      SelectableText(
                        '\$${values["newPrice"]}',
                        onTap: () => copyAndToast(values["newPrice"]),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 12),
              ],
            ),
          ),
        );
      },
    );
  }
}
