import 'package:flutter_bloc/flutter_bloc.dart';

class ValuesCubit extends Cubit<Map<String, dynamic>> {
  ValuesCubit()
      : super(
          {
            'percentage': 5,
            'currentPrice': 0,
            'increasedBy': 0,
            'targetPrice': 0,
            'from': 0,
            'to': 0,
            'percentageDifference': 0,
            'newPrice': 0,
          },
        );

  //
  void updateCurrentPrice(value) {
    double _value = double.tryParse(value) ?? 0;
    state['currentPrice'] = _value;
    calculatePrices();
  }

  //
  void updatePercentage(value) {
    double _value = double.tryParse(value) ?? 0;
    state['percentage'] = _value;
    calculatePrices();
  }

  //
  void updateFrom(value) {
    double _value = double.tryParse(value) ?? 0;
    state['from'] = _value;
    calculatePercentage();
  }

  //
  void updateTo(value) {
    double _value = double.tryParse(value) ?? 0;
    state['to'] = _value;
    calculatePercentage();
  }

  void calculatePrices() {
    state['increasedBy'] =
        (state['currentPrice'] * (state['percentage'] / 100));
    state['targetPrice'] = state['currentPrice'] + state['increasedBy'];
  }

  void calculatePercentage() {
    var percent = (state['to'] / state['from']) * 100 - 100;
    state['percentageDifference'] = double.parse(percent.toStringAsFixed(2));
    state['newPrice'] = double.parse(
        (state['from'] + (state['from'] * (percent / 100))).toStringAsFixed(2));
  }
}
